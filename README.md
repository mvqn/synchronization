# mvqn/collections
A library for collections, specifically designed for use in MVQN projects, but generic enough for anything!

## Installation
Install the latest version with
```bash
composer require mvqn/common
```

## Basic Usage
```php
<?php

// COMING SOON
```

## Documentation

COMING SOON!

## Third Party Packages
COMING SOON

## About

### Requirements
COMING SOON!

### Related Packages
[mvqn/annotations](https://github.com/mvqn/annotations)\


### Submitting bugs and feature requests
Bugs and feature request are tracked on [Github](https://github.com/mvqn/common/issues)

### Author
Ryan Spaeth <[rspaeth@mvqn.net](mailto:rspaeth@mvqn.net)>

### License
This module is licensed under the MIT License - see the `LICENSE` file for details.

### Acknowledgements
COMING SOON!
